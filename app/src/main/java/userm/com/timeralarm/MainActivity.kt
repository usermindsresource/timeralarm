package userm.com.timeralarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.net.nsd.NsdManager
import android.net.wifi.p2p.WifiP2pManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


private const val LOG_TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private lateinit var nsdManager: NsdManager
    private var receiver: BroadcastReceiver? = null
    private lateinit var channel: WifiP2pManager.Channel
    private lateinit var manager: WifiP2pManager
    private val SERVICE_TYPE = "_services._dns-sd._udp"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(LOG_TAG, "Main Activity Started")
        setContentView(R.layout.activity_main)
        sync.setOnClickListener { onClicked(it) }
        reset.setOnClickListener { onClicked(it) }
        manager = getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager
        channel = manager.initialize(this, mainLooper, null)
        nsdManager = this.getSystemService(Context.NSD_SERVICE) as NsdManager
//        createNsdService()
//        discover()
    }

    private fun createNsdService(){
        val nsdReg = NsdServiceRegister(nsdManager)
        nsdReg.initializeServerSocket()
    }

    private fun discover() {


        // Instantiate a new DiscoveryListener
        val discoveryListener = NsdDiscoveryListener(nsdManager)

        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener)


//        manager.discoverPeers(channel, object : WifiP2pManager.ActionListener {
//
//            override fun onSuccess() {
//                Log.d(LOG_TAG, "discover peers On Success")
//            }
//
//            override fun onFailure(reasonCode: Int) {
//                Log.d(LOG_TAG, "discover peers On failure")
//            }
//        })
    }

//    private fun registerBroadcast() {
//        receiver = BroadcastListener(manager, channel, this)
//        val filters = IntentFilter().apply {
//            addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)
//            addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)
//            addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)
//            addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
//        }
//        registerReceiver(receiver, filters)
//    }

    /** register the BroadcastReceiver with the intent values to be matched  */
    public override fun onResume() {
        super.onResume()
//        registerBroadcast()
    }

    public override fun onPause() {
        super.onPause()
//        unregisterReceiver(receiver)
    }

    private fun onClicked(view: View) {
        when (view.id) {
            R.id.sync -> {
                Log.d(LOG_TAG, "Sync is clicked")
                discover()
            }
            R.id.reset -> {
                Log.d(LOG_TAG, "Reset is clicked")
                createNsdService()
            }
        }
    }


}
