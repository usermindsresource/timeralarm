package userm.com.timeralarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pManager
import android.util.Log

private const val LOG_TAG = "BroadcastListener"

class BroadcastListener(
    manager: WifiP2pManager,
    channel: WifiP2pManager.Channel,
    mainActivity: MainActivity
) : BroadcastReceiver() {
    private val manager: WifiP2pManager = manager
    private val channel: WifiP2pManager.Channel = channel
    private val activity: MainActivity = mainActivity
    private val peers = mutableListOf<WifiP2pDevice>()

    private val peerListListener = WifiP2pManager.PeerListListener { peerList ->
        val refreshedPeers = peerList.deviceList
        Log.d(LOG_TAG, "devices found : ${peerList.deviceList.size}")
        peerList.deviceList.forEach { device->
            Log.d(LOG_TAG, "${device.status} ${device.deviceName}")
        }
        if (refreshedPeers != peers) {
            peers.clear()
            peers.addAll(refreshedPeers)

        }

        if (peers.isEmpty()) {
            Log.d(LOG_TAG, "No devices found")
            return@PeerListListener
        }
    }
    override fun onReceive(context: Context, intent: Intent) {
        Log.d(LOG_TAG, "Action Received : ${intent.action}")
manager.requestPeers(channel, peerListListener)

    }
}

