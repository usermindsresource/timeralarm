package userm.com.timeralarm

import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.util.Log

private const val LOG_TAG = "NsdDiscoveryListener"

class NsdDiscoveryListener(nsdManager: NsdManager) : NsdManager.DiscoveryListener {
private val nsdManager = nsdManager


    // Called as soon as service discovery begins.
    override fun onDiscoveryStarted(regType: String) {
        Log.d(LOG_TAG, "Service discovery started")
    }

    override fun onServiceFound(service: NsdServiceInfo) {
        // A service was found! Do something with it.
        Log.d(
            LOG_TAG,
            "Service discovery success   $service"
        )

    }

    override fun onServiceLost(service: NsdServiceInfo) {
        // When the network service is no longer available.
        // Internal bookkeeping code goes here.
        Log.e(LOG_TAG, "service lost: $service")
    }

    override fun onDiscoveryStopped(serviceType: String) {
        Log.i(LOG_TAG, "Discovery stopped: $serviceType")
    }

    override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
        Log.e(LOG_TAG, "Discovery failed: Error code:$errorCode")
        nsdManager.stopServiceDiscovery(this)
    }

    override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
        Log.e(LOG_TAG, "Discovery failed: Error code:$errorCode")
        nsdManager.stopServiceDiscovery(this)
    }

}